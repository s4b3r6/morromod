# Plague Rat

## Version: 2

This mod adds a plague rat to the game that will haunt the player.

Every time a plague rat spawns, it frenzies everything around the player.

Every time a plague rat dies, there is a chance two more will appear.

The mod file is [here](/static/PlagueRat.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).

---

## Too many rats?

Adjust plaguerat_max to fit your preferences. Default is 20.

e.g.

```set plaguerat_max to 10```.
