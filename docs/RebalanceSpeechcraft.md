# Rebalance Speechcraft

## Version: 1

This rebalances speechcraft, making persuasion give a little more XP and be a little easier.

The mod file is [here](/static/rebalance_speechcraft.omwaddon).

This mod has no dependencies.


It is covered by the [LICENSE](/static/LICENSE.html).
