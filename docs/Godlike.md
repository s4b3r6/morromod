# Godlike

## Version: 3

This mod changes gameplay ever so slightly.

When the Heart is destroyed, you are granted abilities I think fitting for someone attempting to use the Heart.

If you can't wait for the quest, or you've already done it, run:

```
StartScript chim_asccend
```

## Incoming:

* Finish upgrading all skills to god-level
* Journal entry
* More Godlike Commands
    * Banish
    * Freeze in time
* Resurrection of the dead?
* Godlike Regalia
    * Ring of Fast Travel
    * Ring of Sex Change
    * Ring of Species Change
* Vivec should treat you as an equal
* All the factions should have generic worship-like phrasing
* Optional Tribunal patch
    * Almalexia might mention your ascension
    * Tribunal Ordinators should show more respect
* Optional Bloodmoon patch
    * Guards should show more respect
    * Don't tweak the Nords. Why would they care?

The mod file is [here](/static/Godlike.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).
