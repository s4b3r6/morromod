# Poison Darts

## Version: 1

Adds some poison darts to the game. Recommended for thieves and assassins. Adds:

* Dart of Confusion (`dart_confusion`), which befuddles mages (non-lethal)
* Dart of Paralyse (`dart_paralyse`), which stops poeple in their tracks (non-lethal)
* Dart of Quiet (`dart_quiet`), which calms people down (non-lethal)
* Poisoned Dart (`dart_poison`), which slowly kills people (lethal)

Darts are found scattered around the world. Try looking for thieves and assassins.


The mod file is [here](/static/poison_darts.omwaddon).

This mod has no dependencies.


It is covered by the [LICENSE](/static/LICENSE.html).
