# Mudcrabs Want It All

## Version: 1

There is a rumour, of a mudcrab buying one or two things, in the middle of nowhere.

... This mod tweaks such a creature, letting it buy anything that it might desire.

---

The mod file is [here](/static/MudcrabsWantItAll.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).
