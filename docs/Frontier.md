# Frontier

## Version: 52

***THIS MOD IS NOT READY***.

***CAUTION:*** If you install this mod, it may break future updates to itself. So, ***DO NOT SAVE WHILST IT IS ACTIVE***.

This mod adds a little interactive steading near Seyda Neen.

It is similar to Skyrim's Hearthfire DLC or it's inspiration the Lakeview mod.

---

![Screenshot](/static/Frontier.png)

---

## Bugs

* Skooma prices mean that brewing every scrap of moon sugar can make you a millionaire.
* Time measurements ocassionally become negative.

---

## Changelog

* v52 - Steel armor is now forgeable.
* v51 - Tree now uses our forged steel axe, and one is hidden nearby.
* v50 - Ability to sell wood, market chest tells the player (once) how it works.
* v49 - Market has minimum prices to sell at now for most items.
* v48 - Implemented forging steel weapons.
* v47 - Market only pays out a maximum of once per day.
* v46 - Lower Basement implemented.
* v45 - Trapdoor made to look nicer.

---

## Hopes

* A house-building mini-quest. (Use a chest to build access to a cell.)
    * In-progress
    * Basement
        * Crafting tools - Keg, Forge, etc.
    * Sub-basement
        * Portals to Mage's Guild, ala Frostcrag Spire (implemented)
        * Regrowing herb garden, ala Frostcrag Spire?
* The bane of my existence - a Forge (or series of forges) for making weapons, armor, etc.
    * in progress
    * Alternatively, use three menus:
        * Base metal
        * Weapon, armor, etc.
        * Category (dagger, longsword, etc.)
* Craftable Smelter
* Craftable bed (once you have house)
* Craftable chest (once you have house)
* Craftable wardrobe (once you have house)
* Craftable table (once you have house)
* Craftable chairs (once you have house)
* Craftable oven (once you have house, for indoors)
* Craftable keg (once you have house, for indoors)

The mod file is [here](/static/Frontier.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).
