# Rebalance Guards

## Version: 1

Guards always report you, even if they have no idea where you are, and die in one hit.

This mod attempts to fix that, whilst keeping guards responsive to most crimes.

The mod file is [here](/static/rebalance_guards.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).

<small>Caution: This mod modifies every bow and every arrow. It could clash with other mods.</small>
