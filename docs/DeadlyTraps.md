# Deadly Traps

## Version: 1

This cranks up the effects of traps to make them deadly. All trap effects last longer, and are much more powerful.

The mod file is [here](/static/deadly_traps.omwaddon).

This mod has no dependencies.


It is covered by the [LICENSE](/static/LICENSE.html).
