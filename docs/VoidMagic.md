# Void Magic

## Version: 1

Adds some Dishonored-esque Void magic. Particularly suited to a thief or assassin. Added are:

* Devouring Swarm (`ring_of_devouring_swarm`)
* Blink (`ring_of_blink`)

Blink is based on [Ring of Hop](/RingOfHop) and may conflict. Use one or the other! Also be careful with Devouring Swarm: you can in principle summon an enormous number of rats which can hurt performance. Note that the other Void-style spells are inbuilt (e.g. Shadow Kill's equivalent is 'remove corpse'; Blood Thirsty can be achieved with absorb spells) or not implementable afaict.

Both spells have a skill-based range. Try increasing Willpower or Endurance.

Bugs:

* Sometimes Devouring Swarm will not spawn.
* Sometimes Devouring Swarm will attack you.
* Sometimes Devouring Swarm will not attack your enemies.


The mod file is [here](/static/VoidMagic.omwaddon).

This mod depends on:

* Tribunal


It is covered by the [LICENSE](/static/LICENSE.html).
