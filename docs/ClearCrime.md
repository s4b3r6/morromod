# Clear Crime

## Version: 1

In trouble?

More trouble than you can bribe your way out of?

Go talk to Arille!

The mod file is [here](/static/ClearCrime.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).
