# Ring of Fast Travel

## Version: 1

Sometimes, moving around Morrowind is tedious.

This is a little cheat item, a ring that when you wear it, gives you some teleport options.

Add it to your player with:

```
player->additem "ring of fast travel" 1
```

The mod file is [here](/static/RingOfFastTravel.omwaddon).

This mod has no dependencies.

It is covered by the [License](/static/LicenseRingOfFastTravel.html).
