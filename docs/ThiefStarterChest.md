# Thief Starter Chest

## Version: 1

Adds a chest full of goodies for a thief near Seyda Neen. There is a short quest to help you find it; ask for rumours, someone may know something.

The mod file is [here](/static/ThiefStarterChest.omwaddon).

There are also extra mods, which depend on the main mod:

* [Thief Starter Chest Extras](/static/ThiefStarterChestExtras.omwaddon) adds support for [University](/University), [Poison Darts](/PoisonDarts), and [Void Magic](/VoidMagic).
* [Thief Starter Chest Assassin Extras](/static/ThiefStarterChestAssassinExtras.omwaddon) adds some extra goodies to the chest for assassins.


This mod depends on:

* Tribunal


It is covered by the [LICENSE](/static/LICENSE.html).
