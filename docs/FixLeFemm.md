# Fix LeFemm

## Version: 1

LeFemm Armor is an official plugin (mini-mod) for Morrowind, released by Bethesda.

Unfortunately, one of the sellers, Sirollus Saccus in Ebonheart has taken such a liking to the golden armor, they wear it rather than selling it, forcing you to kill or pull off a remarkable pickpocket (after damaging the armor so he'll stop wearing it).

This mod fixes that by putting more in his inventory. He'll still wear it, which looks a little odd, but you can also purchase it.

The mod file is [here](/static/fixlefemm.omwaddon).

This mod depends on:

* LeFemm Armor ([Made available for free by Bethesda](https://cdn.bethsoft.com/elderscrolls/morrowind/other/lefemmarmor1.1.zip))

It is covered by the [LICENSE](/static/LICENSE.html).
