# Adventurer's Lantern

## Version: 1

Adds an actual useful lantern. It is bright, has a large radius, and lasts a while. It is sold by many merchants (in particular, those which stock and buy lights) and also scattered throughout the wilderness and dungeons. Or console it to yourself: `adventurers_lantern`.

The mod file is [here](/static/adventurers_lantern.omwaddon).

This mod has no dependencies.


It is covered by the [LICENSE](/static/LICENSE.html).
