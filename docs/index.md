# Morromod

This site hosts a series of small, but very useful, mods for the Bethesda game, [Morrowind](https://elderscrolls.bethesda.net/en/morrowind), using the [OpenMW](http://openmw.org/) engine.

Why is OpenMW required as well?

* OpenMW runs on Windows, Linux and macOS, Xbox | Morrowind was just Windows & Xbox
* OpenMW banishes some of the scripting limitations, making more extensive and powerful mods possible

## Featured Mods

* [Frontier](/Frontier) - An interactive steading.
* [University](/University) - Enroll to skip the grind.
* [Spellbooks](/spellbooks) - Add spellbooks to the game, as a new mechanic.

## Licensing

This site is covered under the [LICENSE](/static/LICENSE.html).

However, individual mods may list differing licenses on their own page to cover themselves.
