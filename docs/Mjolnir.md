# Mjölnir

## Version: 1

This adds a powerful hammer, inspired by Mjölnir. It is a one-handed blunt weapon with a strong shock attack on strike. The shock attach has a cooldown of a several seconds, during which time the hammer is mostly useless. Be careful with equipping, picking up, and dropping `mjolnir` and `mjolnir_dummy` because you can break the script and potentially create duplicates. Just give yourself `mjolnir` and you should be right.

The mod file is [here](/static/mjolnir.omwaddon).

I am currently testing an alternative which includes a small quest to recover Mjölnir. I believe it is fine as it is, but there may be some undiscovered bugs or mechanics which need tweaking. Start the quest by resting. Note that there may be a bug in picking up the hammer --- please let me know if this persists. This version is [here](/static/mjolnir_quest.omwaddon).

Only use one mod at a time; the combination may result in undefimed behaviour.

This mod depends on:

* Tribunal
* Bloodmoon

It is covered by the [LICENSE](/static/LICENSE.html).
