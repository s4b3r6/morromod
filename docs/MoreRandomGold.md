# More Random Gold

## Version: 1

Bumps up the values for random gold found scattered in the wilderness and dungeons. Because I want more money, more quickly.

The mod file is [here](/static/more_random_gold.omwaddon).

This mod has no dependencies.


It is covered by the [LICENSE](/static/LICENSE.html).
