# Ring of Deadpool

## Version: 1

This is a cheat mod.

The ring when worn boosts your health, and gives you ludicrous health regeneration abilities, just like the namesake.

Technically, it may be possible to be killed whilst wearing it, but I have only managed it ocassionally, such as falling from the roof of the world.

To give it to yourself:

```
player->additem "ring of deadpool" 1
```

The mod file is [here](/static/RingOfDeadpool.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).
