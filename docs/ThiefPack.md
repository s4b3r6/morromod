# Thief Pack

# Version: 1

This is a collection of mods to enhance playing as a thief. It is currently under testing and development. Included should be:

* [Ring of Theft](/RingOfTheft)
* [Clear Crime](/ClearCrime)
* [Thief Starter Chest](/ThiefStarterChest) which depends on
	* [University](/University)
	* [Poison Darts](/PoisonDarts)
	* [Void Magic](/VoidMagic)

See respective pages for details.
