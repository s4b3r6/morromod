# Ring of Icarian Flight

## Version: 1

This creates a ring with the same effect as... Let's call him Icarus. You can stumble onto him near Seyda Neen.

This ring is fairly game-breaking.

However, whilst wearing the ring, you will probably not die after jumping, surprisingly.

The mod file is [here](/static/RingOfIcarianFlight.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).
