# Fix Balmora Fighter's Guild Door

## Version: 1

The front door to the Balmora Fighter's Guild teleports you to a position that is a little disorienting. This mod tweaks the location you get teleported to making it slightly easier to orient yourself.

The mod file is [here](/static/fix_balmora_fg_door.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).
