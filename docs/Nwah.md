# Nwah

## Version: 1

Ever wanted to just go around picking fights? So your companion can murder someone for you?

No?

Oh. Then this isn't for you.

This mod adds the insult, "You N'wah!" to your repetoire.

The mod file is [here](/static/Nwah.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).
