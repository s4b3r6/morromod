# Rebalance Mercantile

## Version: 1

This rebalances mercantile, making bargaining close to the actual item value a little easier. It also grants a little more XP for successful bargaining.

The mod file is [here](/static/rebalance_mercantile.omwaddon).

This mod has no dependencies.


It is covered by the [LICENSE](/static/LICENSE.html).
