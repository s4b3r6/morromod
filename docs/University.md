# University

## Version: 1

Early games can be a bit of a grindfest, which isn't especially fun. This mod adds an effective university. At Hlaalu Manor in Balmora is a Dean who will enroll you in a course (thief, warrior, mage, business) which will boost your skills to a little better than average. You can either pay or use the Letter of Introduction (`letter_of_introduction`) to enroll. You can only enroll in one class, which will only take a few seconds in real time to finish.

I plan to add some complementary mods which will include a short quest to gain a Letter.

The mod file is [here](/static/university.omwaddon).

This mod has no dependencies

It is covered by the [LICENSE](/static/LICENSE.html).
