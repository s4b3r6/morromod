# Onslave

# Version: 0

This is an ambitious mod that will take some time to create.

It's in proposal and prototyping stage, so no files yet.

## Proposal:

Slavery in Morrowind is an idea that Bethesda didn't fully explore, probably because of how inflamatory a subject it is.

However, it could be something deeply cultural, full of lore, and experiences that can change how you play Morrowind.

This mod intends to present options that will allow you to experience both sides of the coin, so to speak.

* Be a brutal slave overlord
* Free all the slaves
* Enslave the rich and evil
* Enslave the poor and destitute
* Deal with the management of a plantation

### Phase One:

* Add ability to _pick_ a slave's bracers, and thus release them.
* Add ability to _remove_ a slave's bracers with magic, and thus release them.


### Phase Two:

* Have slaves carry your burdens. (Access to their inventory.)
* Have slaves force-equip items in their inventory.
* Be able to transport _with_ you to a location, rather than getting abandoned if you cast Almsivi Intervention.

### Phase Three:

* A plantation you can purchase/steal.
* Ability to enslave near-defeated enemies.
* Ability to send a slave to work on the plantation.

### Phase Four:

* Tie plantation into a questline or two with the Twin Lamps.
* Income from the plantation.
    * Gold or maybe Product (based on number of slaves and slave morale)
    * Maybe a traderoute establishment questline or two
* Slave riots on the plantation (based around a morale system).
    * If a riot occurs, add a journal entry
    * Riot can be put down with extreme prejudice
    * Riot can be stopped by caving to demands
    * Riot can be stopped by freeing all slaves
* Ability to freely release plantation slaves.
* Ability to improve morale on the plantation.
    * Put down rebellion with violence.
    * Improve living conditions, etc.
