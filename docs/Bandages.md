# Bandages

## Version: 2

Adds two bandages, small and large, to complement potions and spells for healing yourself. The small bandage will heal a small random amount and the large will heal a large random amount. Both will make you weak, cursing you with a weakness to disease and poison, and draining health and fatigue ('bleeding') every few seconds. This bleeding is removed once you rest. Consider pairing with a camping mod, such as [this](https://www.nexusmods.com/morrowind/mods/42919/?tab=description).

Bandages can be found in a 'red cross' chest in all temples. These chests are refilled every few days.

Note 1: bandages are implemented as lights so they reliably stop working after some time. You will have to heal in the dark.
Note 2: I think everything is working now, I'm happy to take bug reports.


The mod file is [here](/static/bandages.omwaddon).

This mod has no dependencies.


It is covered by the [LICENSE](/static/LICENSE.html).
