# Unbalance Archery

## Version: 1

Archery is Morrowind is kinda disappointing, after Skyrim.

The best arrow, with the best random roll, can only do about 15 damage. Which is something any rogue can shrug off.

This mod instead overpowers every bow and every arrow. So sneak archer becomes possible.

Arrows are also now weightless, like later games.

The mod file is [here](/static/unbalance_archery.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).

<small>Caution: This mod modifies every bow and every arrow. It could clash with other mods.</small>
