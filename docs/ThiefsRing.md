# Ring of Theft

## Version: 1

This is a somewhat-balance ring that would fit with a thief playthrough.

It is a constant-effect ring.

Pros:

* Chameleon 70-100
* Silence 70-100

Cons:

* Burden 10-50
* Damage Health 10-20
* Damage Speed 20-99

Upshot: You become a better thief whilst wearing it, but it may kill you. And if you get caught, you won't be charming anyone.

---

Add it to your character with:

```
player->additem ringoftheft 1
```

---

The mod file is [here](/static/TheifsRing.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).
