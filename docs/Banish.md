# Banish

## Version: 1

This is a cheat-y kind of mod.

It adds two spells to the game, you can add them by running:

    player->addspell banish

    player->addspell statue

The Statue spell is a cheap-to-cast spell that immediately immobilises an NPC for 3000 seconds, whilst also making them damn near immortal (Single-shot kill overpowered weapons may still kill them).

The Banish spell is the same as Statue, except it also makes the NPC invisible.

The idea is, if you encounter an enemy you are not yet prepared to face, you can freeze them, and then come back to them later when you're prepared for this level of a fight.

The mod file is [here](/static/Banish.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).
