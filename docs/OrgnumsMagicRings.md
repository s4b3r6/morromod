# Orgnum's Magic Rings

## Version: 1

Some artifacts from the great wizard Orgnum have been discovered in Vvardenfell. Alas, no hand but his can wield them safely, so one must be careful wielding them...

A working project, this mod adds several powerful, balanced magic rings. I may add a quest at some point. These are:

* Detect Key (`ring_orgnum_detect_key`)
* Feather (`ring_orgnum_feather`)
* Icarian Flight (`ring_orgnum_icarian_flight`)
* Open (`ring_orgnum_open`)
* Water Crossing (`ring_orgnum_water_crossing`)

The first 4 are little more than stronger versions of what can be found or enchanted in the game. All have some appropriate curse so you don't abuse the power. The first 3 are constant effects and the last 2 must be cast. The last deserves a little more detail: crossing bodies of water is a pain, this ring resolves that by granting water walk and a string jump. Boing.


The mod file is [here](/static/orgnums_magic_rings.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).

Additionally, there is a Mark and a Recall rings for use with [multi mark](https://www.nexusmods.com/morrowind/mods/44825?tab=description) --- the built in Mark and Recall enchantments don't seem to work with multi mark. These rings are not cursed. The mod file is [here](/static/orgnums_mark_and_recall.omwaddon). This mod depends on

* Tribunal
* Bloodmoon
* Multiple Teleport Marking


It is covered by the [LICENSE](/static/LICENSE.html).
