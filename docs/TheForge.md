# The Forge

## Version: 13

## Status

This mod is deprecated in favour of [Frontier](/Frontier).

This mod never reached a mature stage.

I cannot be blamed if it crashes your game and corrupts both it and OpenMW.

Note: I am currently playing with the values of ores and ingots, to try and balance things out a bit.

## About

This mod adds an entirely new element to the game.

Blacksmithing.

Now, not only can you learn to repair things, you can learn to make them.

This adds a new source of economy to the game, as well as a new skill to master.

### Currently:

* An anvil in the courtyard at Balmora (Temporary Location)
    * The anvil cannot be operated unless you have an Armourer's Hammer.
* A smelter in the courtyard at Balmora (Temporary Location)
    * The smelter cannot be operated unless you have a Blacksmith's Glove (ID: smith_glove).
* A book of recipes. (ID: smithbook)
    * Everything in the book is implemented.
* A book of jewelry recipies. (ID: smith_jewelry_book)
    * Everything in the book is implemented.
    * This covers all the base rings and amulets.
* A way to convert scrap metal to iron ore (which is also new).
* A way to convert any ore into ingots.
* Several new objects for exclusive use in smithing.
* A smithing skill. (Max of 100).
* Scripting to prevent grinding the smith skill. (Loss of XP on repeated attempts on the same item.)
* Ores should be able to be found in the wild as well.
* Gemstones (apart from pearls) can be found alongside ores, meaning you won't have to farm dremora.
* All Battle and War axes.
* All clubs.
* All maces.
* All warhammers.
* All staffs.
* All daggers.

### Planned:

* Smith skill will cap so that the hardest items still have a chance of failing for the best smith.
* Any metal misc item should be able to be broken down.
* Construct any unenchanted weapon or armour.
* Construct lockpicks, probes, etc. (Combination of lockpick skill and smith skill).
* Item value dependent on smith skill.
* Add anvils to multiple locations throughout the world.
* Maybe a quest item for a master smith. (Generally a master sword or axe, in lore).
    * Upon achieving skill of max, add journal entry and MessageBox it.

## Download

The mod file is [here](/static/Forge.omwaddon).

## Dependencies

This mod has no dependencies.

## License

It is covered by the [LICENSE](/static/LICENSE.html).
