# Rebalance PickPocket

## Version: 1

Pickpocket in Morrowind is broken. It's near impossible to do in the normal game.

This rebalances it.

The mod file is [here](/static/rebalance_pickpocket.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).
