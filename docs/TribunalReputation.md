# Tribunal Reputation

## Version: 1

The introduction to Tribunal can be a bit harsh.

The introduction is an attack by a Dark Brotherhood assassin, except it's based on a timer rather than anything else, which means a player with level 1 skills may encounter an assassin nearly every time they sleep, which just leads to them getting slaughtered.

This mod balances it by doing a reputation check (which fits the lore of why you're being attacked).

You must earn a reputation of at least 10 before the assassins start coming out of the woodwork.

The mod file is [here](/static/changerep.omwaddon).

This mod depends on:

* Tribunal

It is covered by the [LICENSE](/static/LICENSE.html).
