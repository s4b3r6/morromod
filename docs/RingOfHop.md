# Ring of Hop

## Version: 1

Adds a magic ring (`ring_of_hop`) which when cast will cause the player to teleport ('hop') a small distance forwards. Be careful because collision detection is for nerds so you can break things. Also there's some small anisotropy in the direction you hop. I think I've fixed this but may revisit.

You may see this repeated and tweaked in the upcoming Thief/Assassin mod pack.


The mod file is [here](/static/ring_of_hop.omwaddon).

This mod depends on:

* Tribunal


It is covered by the [LICENSE](/static/LICENSE.html).
