# Spellbooks

## Version: 1

This mod adds a series of books, that when opened, grant you a spell.

You have a 20% chance of failure when opening the book. If you fail, the book disintegrates, and the spell goes haywire and does something bad to you. Survivable, if you have the skill.

Spellbooks can be found far and wide throughout Morrowind, but you can also give them to yourself manually.

(They turn up as loot, and at booksellers).

To add a book to your player:

```
player->additem sbk_intervention 1
```

All spellbook IDs start with ```sbk```.

---

The mod file is [here](/static/spellbooks.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).

---

Spell coverage:

* Divine Intervention
* Levitate
* Ondusi's Open Door
* Mark/Recall
* Blessed Touch
* Summon Bonelord
* Summon Bonewalker
* Summon Clannfear
* Summon Daedroth
* Summon Dremora
* Summon Flame Atronach
* Summon Frost Atronach
* Summon Golden Saint
* Summon Greater Bonewalker
* Summon Hunger
* Summon Scamp
* Summon Skeletal Minion
* Summon Storm Atronach
* Summon Winged Twilight
* Summon Ancestral Ghost
* Turn Undead
* Water Breathing
* Water Walking
* Burden
* Fireball
* Frostball
* Black Hand
* Clench
* Fleabite
* Spirit Knife
* Soulpinch
* Armor Eater
* Weapon Eater
* Clumsy Touch
* Distraction
* Enervate
* Sleep
* Drain Blood
* Flay Spirit
* Shock
* Stormhand
* Weakness to Common Disease
* Icaran Flight
* Weakness to Fire
* Dire Weakness to Frost
* Dire Weakness to Magicka
* Dire Weakness to Poison
* Dire Weakness to Shock
* Crying Eye
* Calm Creature
* Calming Touch
* Chameleon
* Charming Touch
* Demoralize Beast
* Demoralize Humanoid
* Frenzy Beast
* Frenzying Touch
* Hide
* Light
* Night-Eye
* Paralysis
* Rally Beast
* Rally Humanoid
* Sotha's Grace
* Silence
* Earwig
* Absorb Agility
* Absorb Endurance
* Absorb Intelligence
* Absorb Luck
* Absorb Personality
* Absorb Speed
* Absorb Strength
* Absorb Willpower
* Crimson Despair
* Absorb Fatigue
* Righteousness
* Almsivi Intervention
* Detect Creature
* Detect Enchantment
* Tevral's Hawkshaw
* Purge Magic
* Almalexia's Grace
* Sotha's Mirror
* Soul Trap
* Spell Absorption
* Telekinesis
* Rilm's Gift
* Cure Common Disease
* Free Action
* Balyna's Antidote
* Turn of the Wheel
* Charisma
* Enrichment
* Vitality
* Powerwell
* Poet's Whim
* Resist Common Disease
* Seryn's Blessing
* Variable Resist Corprus Disease
* Resist Fire
* Flameguard
* Resist Frost
* Frostguard
* Resist Magicka
* Magickguard
* Resist Paralysis
* Resist Poison
* Poisonguard
* Resist Shock
* Shockguard
* Restore Agility
* Rest of St. Merris
* Balyna's Soothing Balm
* Mother's Kiss
* Burden of Sin
* Feather
* Fire Barrier
* Frost Barrier
* Tinur's Hoptoad
* Shock Barrier
* Fenrick's Doorjam
* Shield
* First Barrier
* Slowfall
* Buoyancy
* Vivec's Kiss
* Water Walking
