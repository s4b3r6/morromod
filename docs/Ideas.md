# Ideas

These are mods I'd like to implement, but may not have time, or motivation to do so yet:

## Better Slaves

Slaves currently exist only so you can free them.

However, you should (but currently can't) be able to:

* Give items to carry/wear
* Order to wait, follow, or goto
* Give food to increase skills
* Choose what they wear ([Enforced clothing for a companion](http://wiki.theassimilationlab.com/mmw/Morrowind_Script_Library#Enforced_clothing_for_a_companion))

I want to take the whole slavery thing to a new level. I want to be able to free all the slaves. And start a slave rebellion. Or, to enslave anyone, and make them work my farm.

## Mimic

A chest that isn't a chest that might attack you.

Non-quest vital chests should have a 50/50 chance of being a mimic instead.

## Travelling Merchant

A merchant that travels/teleports to the major cities on a daily schedule.

The merchant should sell everything, and be able to do things like spells and repairs.

## Own House

A house you can buy, instead of quest for. Probably in Balmora, because I like the city.

## Teleport Spells

Spells that don't kill your enemy, but displace them, temporarily or permanently.

## Runes

Trap spells that you can place on the ground.

Maybe combine [this](http://wiki.theassimilationlab.com/mmw/Morrowind_Script_Library#Hurt_actor_on_contact_2) and [this](http://wiki.theassimilationlab.com/mmw/Morrowind_Script_Library#Invisible_activator).

## Fix LeFemm

Fix LeFemm currently makes a merchant let you buy gold armour.

However, LeFemm also adds female models for: Imperial Steel Cuirass, Imperial Chain Cuirass, Netch Leather Cuirass and Steel Cuirass armors.

But, despite adding the models, they aren't actually implemented. (Tribunal fixes that).

FixLeFemm could do this.

## Ring of Pacification

A ring that calms everyone down no matter what.

Unfortunately, a ring with constant-effect of CalmHumanoid doesn't seem to work.

## Industry

I like the [keg](http://wiki.theassimilationlab.com/mmw/Morrowind_Script_Library#Keg_script) script.

It could be adapted to add all sorts of industry, and therefore economy to Morrowind.

Take features from Stardew and add them to Morrowind? That'd be fun.

We could add new skills to the Player, like smithing or brewing, and effect the outcome that way.

## Nudge (cancelled due to impossibleness)

Sometimes NPCs (especially guards) get in the way and there's no way past. I'd like a ring to teleport them a small distance away --- a nudge.

## Polymorph

It would be nice to have a decent selection of polymorph spells, to temporarily transform an NPC into a docile or weak creature.

Unfortunately, the scripting engine makes it nigh impossible to grab a NPC's actor ID without iterating over everyone.

## School (done)

Training is a bit tedious, but kinda necessary --- it's often very difficult to get started with a skill when it's only 5 or even 20. I want to be able to visit a master, pay then 20,000 gold and in one hit level up a bunch of skills a heap.

## Chained travel

I love the range of travel options. But getting across the map can be a little annoying --- I typically consult UESP to find the required sequence of boats and silt striders to get where I want to. I want some nice UI for chaining together this process. No idea what this should look like.

## Cell based magicka

It'd be cool if magicka wasn't tied to a character but was something inherent to an area. Some areas have lots, some little. And all characters in a given area draw from the same pool.

## Make yourself a god (WIP)

According to lore, Kagrenac's tools can be used to tap the Heart of Lorkhan and make you a god. All you can use the tools for in-game is to destroy it. I want to be able to tap the Heart.

In principle this should be straightforward to implement. I can hook into the `LorkhanHeart` script and implement my own actions when the correct number of strikes is detected. But I want to do this properly, so in addition:

* Need some questline to learn how to tap the heart
* Need to add/update topics so everyone thinks I'm a god
* Need to think about how the religions react --- maybe they run away or fight or have some topic.
* Need to think about how other quests may change if you're a god; for example, it doesn't make sense to be doing guild or house quests as a god.
* Need to think about what powers a god has: massive health, stamina, and magicka; massive regen on former three; buff all attributes; maybe some super awesome spells. Actually, it'd be cool if the player could pick what powers they get.

## Highwayman

A mod to become someone who can accost people on the road.

Something like being able to demand all someone's valuables.

Should affect notoriety and maybe keep you out of the city.

You of course need access to a fence in some cave somewhere.

## Market

The ability to open your own marketstall.

This would be a *huge* mod, requiring the programming of NPCs to supply a flow of income.

But, it would allow a player to take a new direction when playing Morrowind.

## Dishonored-esque Assassin/Thief stuff (WIP)

Dishonored is awesome, and there are corresponding mods for Skyrim. I want something similar, to help a thief/assassin playthrough. We've already touched on some of this (see 'Unbalance Archery', 'Ring of Theft', 'Orgnum's Magic Rings', 'Clear Crime') but I want more. Specifically:

* Effective darts. If I'm a thief, I want to do a no-kill run. If I'm an assassin, I want to do a no-combat run. This means cranking up the strength of darts, and adding poison effects. Should be straightforward. (Done, with room to expand)
* Blink. I need to a short range teleport to get around, as an alternative to Jump. (Done)
* Devouring Swarm. Because death from nowhere is awesome. Should be able to combine Conjouration with the in-development polymorph.  (Done)
- Blood Thirsty. Restore health/fatigue from kills? Might be doable. (Doesn't appear possible)

Dark Vision, Posession, Bend Time, Windblast are probably impossible to implement. Shadow Kill is effectively inbuilt.

## Hop (Done)

So the nudge idea appears impossible, because there's no way to detect that you've touched someone. You can, however, detect effects on yourself. So in principle I could add a short-range (hop) teleport, to jump past NPCs.

## New game boosters

Complementary to University, I want to add some chests with Warrior/Thief/Mage gear near Seyda Neen, also with a Letter of Introduction, and a small quest to find them.
