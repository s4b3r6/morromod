# Secret Words

## Version: 3

---

![Secret Rampage](/static/SecretWords.png)

---

This mod adds a group of secret words, magic words, that you can speak to NPCs for hilarious results.

The mod file is [here](/static/SecretWords.omwaddon).

This mod has some dependency on Tribunal (some words don't work without it).

[//]: # ( htudbneaupyeoo - doombeuponyou, kills )

[//]: # ( kfcffou - fuckyou, makes them jump on the spot )

[//]: # ( tdaoecmbeao - becomeatoad, makes them jump and run )

[//]: # ( onegbe - begone, makes them sneak everywhere )

[//]: # ( sihnrk - shrink, setscale 1/2 )

[//]: # ( owdlmanc - calmdown, setscale 1 )

[//]: # ( wpogur - growup, setscale 2 )

[//]: # ( roerfimst - firestorm, attack me with a firestorm please. )

[//]: # ( volmee - loveme, disposition +100 )

[//]: # ( usaelrflcd - cursedfall, if exterior, teleport skyhigh. If interior, teleport above Balmora Fighter's Guild. )

It is covered by the [LICENSE](/static/LICENSE.html).
