# Sunder Without Keening

## Version: 1

There's a small bug in Morrowind.

If you are a truly godlike character, you can strike and destroy Lorkan's heart. Or it looks that way. Which is somewhat game breaking as you can no longer hit the heart, but it doesn't trigger the end of the main quest.

This mod adds a small change, that if you are a truly godlike figure and can destroy the heart in one blow, the game will progress and not break... But only if you manage to survive the consequences.

The mod file is [here](/static/SunderWithoutKeening.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).
