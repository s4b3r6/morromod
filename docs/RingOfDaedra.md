# Ring of Daedra

## Version: 1

This adds a game-unbalancing constant-effect ring that gives you Daedric armor, longsword and shield.

You can add it to your character with:

```
player->additem "ring of daedra" 1
```

The mod file is [here](/static/RingOfDaedra.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).
