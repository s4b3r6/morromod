# Ring of Caster

## Version: 1

A simple and overpowered ring that will turn you into a master magic caster in no time... So long as your survive the side effects.

(The ring is automatically added to your inventory.)

The mod file is [here](/static/ring_of_caster.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).
