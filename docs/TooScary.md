# Too Scary

## Version: 2

This is a tiny mod.

If you accumulate a truly ridiculous bounty, the Empire will forgive your crimes, rather than giving whoever eventually kills you a king's ransom several times over.

The mod file is [here](/static/TooScary.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).

Changelog:

2 -> Only triggered during sleep.