# Rebalance Fatigue

## Version: 1

This rebalances fatigue slightly reducing the cost of movement. This means that you can recover fatigue by walking, and can freely move around a little longer at lower levels.

The mod file is [here](/static/rebalance_fatigue.omwaddon).

This mod has no dependencies.


It is covered by the [LICENSE](/static/LICENSE.html).
