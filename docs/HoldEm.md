# Hold 'Em

## Version: 1

Allows you to rob any NPC you feel like.

The mod file is [here](/static/TooScary.omwaddon).

This mod has no dependencies.

It is covered by the [LICENSE](/static/LICENSE.html).